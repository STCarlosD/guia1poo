﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Ingenieria : Universitario
    {
        string nombreProyecto;
        int totalHoras;
        int horasCompletadas;

        public string NombreProyecto { get => nombreProyecto; set => nombreProyecto = value; }
        public int TotalHoras { get => totalHoras; set => totalHoras = value; }
        public int HorasCompletadas { get => horasCompletadas; set => horasCompletadas = value; }
    }
}
