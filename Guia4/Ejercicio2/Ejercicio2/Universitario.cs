﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Universitario : Estudiante
    {
        string nombreUniversidad;
        string carrera;
        string materia;
        double nota;
        double cum;

        public string NombreUniversidad { get => nombreUniversidad; set => nombreUniversidad = value; }
        public string Carrera { get => carrera; set => carrera = value; }
        public string Materia { get => materia; set => materia = value; }
        public double Nota { get => nota; set => nota = value; }
        public double Cum { get => cum; set => cum = value; }
    }
}
