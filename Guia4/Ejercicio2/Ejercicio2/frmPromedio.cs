﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio2
{
    public partial class frmPromedio : Form
    {
        public frmPromedio()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmMenu frm = new frmMenu();
            this.Close();
            frm.Show();
        }

        private void frmPromedio_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = frmRegistro.ingenieros;
            double promedio = 0;
            for (int i = 0; i < frmRegistro.ingenieros.Count(); i++)
            {
                promedio += frmRegistro.ingenieros[i].Nota;
            }
            promedio = promedio / frmRegistro.ingenieros.Count();
            label1.Text = "El promedio es : " + promedio;
        }
    }
}
