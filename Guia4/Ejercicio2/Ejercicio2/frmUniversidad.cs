﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio2
{
    public partial class frmUniversidad : Form
    {
        public frmUniversidad()
        {
            InitializeComponent();
        }

        private void frmUniversidad_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = frmRegistro.ingenieros;
            int udb=0, nacional=0, tecno=0;
            for (int i = 0; i < frmRegistro.ingenieros.Count(); i++)
            {
                if (frmRegistro.ingenieros[i].NombreUniversidad == "UDB")
                {
                    udb++;
                }else if (frmRegistro.ingenieros[i].NombreUniversidad == "Nacional")
                {
                    nacional++;
                }else if (frmRegistro.ingenieros[i].NombreUniversidad == "Tecnológica")
                {
                    tecno++;
                }
            }

            if (udb > nacional && udb > tecno)
            {
                label1.Text = "La univerdidad con más ingenieros es UDB";
            }
            if (nacional > udb && nacional > tecno)
            {
                label1.Text = "La univerdidad con más ingenieros es Nacional";
            }
            if (tecno > udb && tecno > nacional)
            {
                label1.Text = "La univerdidad con más ingenieros es Tecnológica";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmMenu frm = new frmMenu();
            this.Close();
            frm.Show();
        }
    }
}
