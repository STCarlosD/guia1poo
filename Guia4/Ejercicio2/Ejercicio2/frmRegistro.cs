﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio2
{
    public partial class frmRegistro : Form
    {
        public frmRegistro()
        {
            InitializeComponent();
        }
        internal static List<Ingenieria> ingenieros = new List<Ingenieria>();

        private void button1_Click(object sender, EventArgs e)
        {
            Ingenieria ing = new Ingenieria();
            ing.Carnet = txtCarnet.Text;
            ing.NivelEstudios = txtNivelEstudios.Text;
            ing.NombreUniversidad = cboUniversidad.SelectedItem.ToString();
            ing.Carrera = txtCarrera.Text;
            ing.Materia = txtMateria.Text;
            ing.Nota = Convert.ToDouble(txtNota.Text);
            ing.Cum = Convert.ToDouble(txtCum.Text);
            ing.NombreProyecto = txtNombreProyecto.Text;
            ing.TotalHoras = Convert.ToInt32(txtHoras.Text);
            ing.HorasCompletadas = Convert.ToInt32(txtHorasCompletadas.Text);

            ingenieros.Add(ing);
            actualizarGrid();
            limpiar();
        }

        void actualizarGrid()
        {
            dgvObjetos.DataSource = null;
            dgvObjetos.DataSource = ingenieros;
        }
        void limpiar()
        {
            txtCarnet.Clear();
            txtNivelEstudios.Clear();
            cboUniversidad.SelectedIndex = -1;
            txtCarrera.Clear();
            txtMateria.Clear();
            txtNota.Clear();
            txtCum.Clear();
            txtNombreProyecto.Clear();
            txtHoras.Clear();
            txtHorasCompletadas.Clear();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            actualizarGrid();
            cboUniversidad.Items.Add("UDB");
            cboUniversidad.Items.Add("Nacional");
            cboUniversidad.Items.Add("Tecnológica");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmMenu frm = new frmMenu();
            this.Close();
            frm.Show();
        }
    }
}
