﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Estudiante
    {
        string carnet;
        string nivelEstudios;        

        public string Carnet { get => carnet; set => carnet = value; }
        public string NivelEstudios { get => nivelEstudios; set => nivelEstudios = value; }
    }
}
