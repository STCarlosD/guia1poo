﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio4
{
    public partial class frmCarro : Form
    {
        public frmCarro()
        {
            InitializeComponent();
        }
        private List<Carro> carros = new List<Carro>();
        public int edit_indice = -1;

        void actualizarGrid()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = carros;
        }
        void limpiar()
        {
            txtMarca.Clear();
            txtModelo.Clear();
            txtMotor.Clear();
            txtLongitud.Clear();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Carro car = new Carro();

            car.Marca = txtMarca.Text;
            car.Modelo = txtModelo.Text;
            car.Motor = txtMotor.Text;
            car.LongitudTuboEscape = Convert.ToDouble(txtLongitud.Text);

            if (edit_indice > -1) //verifica si hay un índice seleccionado
            {
                carros[edit_indice] = car;
                edit_indice = -1;
            }
            else
            {
                carros.Add(car); /*al arreglo de Personas le agrego el objeto creado
                    con todos los datos que recolecté*/
            }
            actualizarGrid();//llamamos al procedimiento que guarda en datagrid
            limpiar();
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow seleccion = dataGridView1.SelectedRows[0];
            int pos = dataGridView1.Rows.IndexOf(seleccion); //almacena en cual fila estoy
            edit_indice = pos; //copio esa variable en índice editado
            Carro car = carros[pos];

            txtMarca.Text = car.Marca;
            txtModelo.Text = car.Modelo;
            txtMotor.Text = car.Motor;
            txtLongitud.Text = car.LongitudTuboEscape.ToString();
        }
    }
}
