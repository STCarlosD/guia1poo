﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio4
{
    public partial class frmMoto : Form
    {
        public frmMoto()
        {
            InitializeComponent();
        }
        private List<Moto> Motos = new List<Moto>();
        public int edit_indice = -1;

        void actualizarGrid()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Motos;
        }
        void limpiar()
        {
            txtMarca.Clear();
            txtModelo.Clear();
            txtMotor.Clear();
            txtTamanio.Clear();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Moto mot = new Moto();

            mot.Marca = txtMarca.Text;
            mot.Modelo = txtModelo.Text;
            mot.Motor = txtMotor.Text;
            mot.TamanioParabrisas = txtTamanio.Text;

            if (edit_indice > -1) //verifica si hay un índice seleccionado
            {
                Motos[edit_indice] = mot;
                edit_indice = -1;
            }
            else
            {
                Motos.Add(mot); /*al arreglo de Personas le agrego el objeto creado
                    con todos los datos que recolecté*/
            }
            actualizarGrid();//llamamos al procedimiento que guarda en datagrid
            limpiar();
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow seleccion = dataGridView1.SelectedRows[0];
            int pos = dataGridView1.Rows.IndexOf(seleccion); //almacena en cual fila estoy
            edit_indice = pos; //copio esa variable en índice editado
            Moto mot = Motos[pos];

            txtMarca.Text = mot.Marca;
            txtModelo.Text = mot.Modelo;
            txtMotor.Text = mot.Motor;
            txtTamanio.Text = mot.TamanioParabrisas;
        }
    }
}
