﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio4
{
    public partial class frmBus : Form
    {
        public frmBus()
        {
            InitializeComponent();
        }
        private List<Bus> buses = new List<Bus>();
        public int edit_indice = -1;

        void actualizarGrid()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = buses;
        }
        void limpiar()
        {
            txtMarca.Clear();
            txtModelo.Clear();
            txtMotor.Clear();
            txtTipo.Clear();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Bus bus = new Bus();

            bus.Marca = txtMarca.Text;
            bus.Modelo = txtModelo.Text;
            bus.Motor = txtMotor.Text;
            bus.Tipo = txtTipo.Text;

            if (edit_indice > -1) //verifica si hay un índice seleccionado
            {
                buses[edit_indice] = bus;
                edit_indice = -1;
            }
            else
            {
                buses.Add(bus); /*al arreglo de Personas le agrego el objeto creado
                    con todos los datos que recolecté*/
            }
            actualizarGrid();//llamamos al procedimiento que guarda en datagrid
            limpiar();
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow seleccion = dataGridView1.SelectedRows[0];
            int pos = dataGridView1.Rows.IndexOf(seleccion); //almacena en cual fila estoy
            edit_indice = pos; //copio esa variable en índice editado
            Bus bus = buses[pos]; 

            txtMarca.Text = bus.Marca;
            txtModelo.Text = bus.Modelo;
            txtMotor.Text = bus.Motor;
            txtTipo.Text = bus.Tipo;
        }
    }
}
