﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio4
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCarro frm = new frmCarro();
            this.Hide();
            frm.Show();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmMoto frm = new frmMoto();
            this.Hide();
            frm.Show();            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmBus frm = new frmBus();
            this.Hide();
            frm.Show();            
        }
    }
}
