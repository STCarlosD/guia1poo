﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ejercicio1
{
    public partial class frmMenu : Ejercicio1.frmBase
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmRegistroEstudiantes frm = new frmRegistroEstudiantes();
            this.Hide();
            frm.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmRegistroDocentes frm = new frmRegistroDocentes();
            this.Hide();
            frm.Show();
        }
    }
}
