﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ejercicio1
{
    public partial class frmRegistroEstudiantes : Ejercicio1.frmRegistro
    {
        public frmRegistroEstudiantes()
        {
            InitializeComponent();
        }

        private void frmRegistroEstudiantes_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private List<Estudiante> Estudiantes = new List<Estudiante>();

        private void actualizarGrid()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Estudiantes;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Estudiante est = new Estudiante();
            est.Nombre1 = txtNombre.Text;
            est.Usuario1 = txtUsuario.Text;
            est.Codigo1 = txtCodigo.Text;

            Estudiantes.Add(est);
            actualizarGrid();
        }
        public int edit_indice = -1;        

        void dgv()
        {
            DataGridViewRow sel = dataGridView1.SelectedRows[0];
            int pos = dataGridView1.Rows.IndexOf(sel);
            edit_indice = pos;
            Estudiante est = Estudiantes[pos];
            txtNombre.Text = est.Nombre1;
            txtUsuario.Text = est.Usuario1;
            txtCodigo.Text = est.Codigo1;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtNombre.Clear();
            txtUsuario.Clear();
            txtCodigo.Clear();
        }
    }
}
