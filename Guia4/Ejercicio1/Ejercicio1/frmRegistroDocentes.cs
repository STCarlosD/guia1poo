﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ejercicio1
{
    public partial class frmRegistroDocentes : Ejercicio1.frmRegistro
    {
        public frmRegistroDocentes()
        {
            InitializeComponent();
        }

        private void frmRegistroDocentes_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        private List<Docente> Docentes = new List<Docente>();
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Docente doc = new Docente();
            doc.Nombre1= txtNombre.Text;
            doc.Usuario1 = txtUsuario.Text;
            doc.Codigo1 = txtCodigo.Text;
            doc.Materia1 = txtMateria.Text;
            Docentes.Add(doc);
            actualizarGrid();
        }
        private void actualizarGrid()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = Docentes;
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtNombre.Clear();
            txtUsuario.Clear();
            txtCodigo.Clear();
            txtMateria.Clear();
        }
    }
}
