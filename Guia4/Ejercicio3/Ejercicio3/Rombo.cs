﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio3
{
    public class Rombo:Figura
    {
        private double dMayor;
        private double dMenor;

        public Rombo(double A, double DMay, double DMen) : base(A)
        {
            dMayor = DMay;
            dMenor = DMen;
        }
        public double DMenor { get => dMenor; set => dMenor = value; }
        public double DMayor { get => dMayor; set => dMayor = value; }

        public override void CalcularArea(Label LR)
        {
            Area = (dMayor*dMenor)/2;
            LR.Text = "Area: " + Area;
        }
    }
}
