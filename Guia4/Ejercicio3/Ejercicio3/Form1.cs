﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            double L = double.Parse(txtLado.Text);
            double A = 0;
            Cuadrado cuad = new Cuadrado(A, L);
            cuad.CalcularArea(lblRespuesta);
        }

        private void btnCalcularCirculo_Click(object sender, EventArgs e)
        {
            double R = double.Parse(txtRadio.Text);
            double A = 0;
            Circulo circ = new Circulo(A, R);
            circ.CalcularArea(lblRespuestaB);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double DMay = double.Parse(txtDmayor.Text);
            double DMen = double.Parse(txtDmenor.Text);
            double A = 0;
            Rombo romb = new Rombo(A, DMay, DMen);
            romb.CalcularArea(lblRespuestaR);
        }
    }
}
