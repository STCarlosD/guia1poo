﻿namespace Ejercicio3
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbCuadrado = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLado = new System.Windows.Forms.TextBox();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.lblRespuesta = new System.Windows.Forms.Label();
            this.lblRespuestaB = new System.Windows.Forms.Label();
            this.btnCalcularCirculo = new System.Windows.Forms.Button();
            this.txtRadio = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblRespuestaR = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtDmayor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDmenor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbCuadrado.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbCuadrado
            // 
            this.tbCuadrado.Controls.Add(this.tabPage1);
            this.tbCuadrado.Controls.Add(this.tabPage2);
            this.tbCuadrado.Controls.Add(this.tabPage3);
            this.tbCuadrado.Location = new System.Drawing.Point(26, 12);
            this.tbCuadrado.Name = "tbCuadrado";
            this.tbCuadrado.SelectedIndex = 0;
            this.tbCuadrado.Size = new System.Drawing.Size(747, 426);
            this.tbCuadrado.TabIndex = 0;
            this.tbCuadrado.Tag = "";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblRespuesta);
            this.tabPage1.Controls.Add(this.btnCalcular);
            this.tabPage1.Controls.Add(this.txtLado);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(739, 400);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Cuadrado";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lblRespuestaB);
            this.tabPage2.Controls.Add(this.btnCalcularCirculo);
            this.tabPage2.Controls.Add(this.txtRadio);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(739, 400);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Circulo";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtDmenor);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.lblRespuestaR);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.txtDmayor);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(739, 400);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Rombo";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lado";
            // 
            // txtLado
            // 
            this.txtLado.Location = new System.Drawing.Point(162, 28);
            this.txtLado.Name = "txtLado";
            this.txtLado.Size = new System.Drawing.Size(100, 20);
            this.txtLado.TabIndex = 1;
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(310, 159);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(95, 35);
            this.btnCalcular.TabIndex = 2;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // lblRespuesta
            // 
            this.lblRespuesta.AutoSize = true;
            this.lblRespuesta.Location = new System.Drawing.Point(252, 297);
            this.lblRespuesta.Name = "lblRespuesta";
            this.lblRespuesta.Size = new System.Drawing.Size(0, 13);
            this.lblRespuesta.TabIndex = 3;
            // 
            // lblRespuestaB
            // 
            this.lblRespuestaB.AutoSize = true;
            this.lblRespuestaB.Location = new System.Drawing.Point(267, 301);
            this.lblRespuestaB.Name = "lblRespuestaB";
            this.lblRespuestaB.Size = new System.Drawing.Size(0, 13);
            this.lblRespuestaB.TabIndex = 7;
            // 
            // btnCalcularCirculo
            // 
            this.btnCalcularCirculo.Location = new System.Drawing.Point(325, 163);
            this.btnCalcularCirculo.Name = "btnCalcularCirculo";
            this.btnCalcularCirculo.Size = new System.Drawing.Size(95, 35);
            this.btnCalcularCirculo.TabIndex = 6;
            this.btnCalcularCirculo.Text = "Calcular";
            this.btnCalcularCirculo.UseVisualStyleBackColor = true;
            this.btnCalcularCirculo.Click += new System.EventHandler(this.btnCalcularCirculo_Click);
            // 
            // txtRadio
            // 
            this.txtRadio.Location = new System.Drawing.Point(177, 32);
            this.txtRadio.Name = "txtRadio";
            this.txtRadio.Size = new System.Drawing.Size(100, 20);
            this.txtRadio.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(75, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Radio";
            // 
            // lblRespuestaR
            // 
            this.lblRespuestaR.AutoSize = true;
            this.lblRespuestaR.Location = new System.Drawing.Point(293, 298);
            this.lblRespuestaR.Name = "lblRespuestaR";
            this.lblRespuestaR.Size = new System.Drawing.Size(0, 13);
            this.lblRespuestaR.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(351, 160);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 35);
            this.button1.TabIndex = 10;
            this.button1.Text = "Calcular";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtDmayor
            // 
            this.txtDmayor.Location = new System.Drawing.Point(203, 29);
            this.txtDmayor.Name = "txtDmayor";
            this.txtDmayor.Size = new System.Drawing.Size(100, 20);
            this.txtDmayor.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(101, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Diagonal Mayor";
            // 
            // txtDmenor
            // 
            this.txtDmenor.Location = new System.Drawing.Point(203, 68);
            this.txtDmenor.Name = "txtDmenor";
            this.txtDmenor.Size = new System.Drawing.Size(100, 20);
            this.txtDmenor.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(101, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Diagonal Menor";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tbCuadrado);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tbCuadrado.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbCuadrado;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label lblRespuesta;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.TextBox txtLado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRespuestaB;
        private System.Windows.Forms.Button btnCalcularCirculo;
        private System.Windows.Forms.TextBox txtRadio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDmenor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblRespuestaR;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtDmayor;
        private System.Windows.Forms.Label label4;
    }
}

