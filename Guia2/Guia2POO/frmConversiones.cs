﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia2POO
{
    public partial class frmConversiones : Form
    {
        public frmConversiones()
        {
            InitializeComponent();
        }
        
        private void cboOpciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtNumero.Text == "" || txtNumero.Text == null || txtNumero.Text == " ")
            {
                MessageBox.Show("Llene correctamente el campo", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);                
            }
            else
            {
                double numero, resultado = 0;
                numero = Convert.ToDouble(txtNumero.Text);
                if (cboOpciones.SelectedItem.ToString() == "grados Celcius a Fanrenheit")
                {
                    resultado = (1.8 * numero) + 32;
                }
                else if (cboOpciones.SelectedItem.ToString() == "metros a pies")
                {
                    resultado = numero * 3.28;
                }
                else if (cboOpciones.SelectedItem.ToString() == "kilogramos a libras")
                {
                    resultado = numero * 2.2;
                }

                txtResultado.Text = resultado.ToString();
            }
            
        }

        private void frmConversiones_Load(object sender, EventArgs e)
        {
            cboOpciones.Items.Add("grados Celcius a Fanrenheit");
            cboOpciones.Items.Add("metros a pies");
            cboOpciones.Items.Add("kilogramos a libras");
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {            
            frmMenu frm = new frmMenu();            
            frm.Show();
            this.Close();
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
    }
}
