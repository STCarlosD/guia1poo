﻿namespace Guia2POO
{
    partial class frmCuadratica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtB = new System.Windows.Forms.TextBox();
            this.lblb = new System.Windows.Forms.Label();
            this.txtC = new System.Windows.Forms.TextBox();
            this.lblc = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.txtA = new System.Windows.Forms.TextBox();
            this.txtX1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtX2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.btnMenu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(156, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(252, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "Formula Cuadrática";
            // 
            // txtB
            // 
            this.txtB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtB.Location = new System.Drawing.Point(273, 70);
            this.txtB.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtB.Name = "txtB";
            this.txtB.Size = new System.Drawing.Size(83, 29);
            this.txtB.TabIndex = 14;
            this.txtB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtB_KeyPress);
            // 
            // lblb
            // 
            this.lblb.AutoSize = true;
            this.lblb.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblb.Location = new System.Drawing.Point(240, 73);
            this.lblb.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblb.Name = "lblb";
            this.lblb.Size = new System.Drawing.Size(21, 24);
            this.lblb.TabIndex = 13;
            this.lblb.Text = "b";
            // 
            // txtC
            // 
            this.txtC.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtC.Location = new System.Drawing.Point(481, 70);
            this.txtC.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtC.Name = "txtC";
            this.txtC.Size = new System.Drawing.Size(83, 29);
            this.txtC.TabIndex = 16;
            this.txtC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtC_KeyPress);
            // 
            // lblc
            // 
            this.lblc.AutoSize = true;
            this.lblc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblc.Location = new System.Drawing.Point(448, 73);
            this.lblc.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblc.Name = "lblc";
            this.lblc.Size = new System.Drawing.Size(20, 24);
            this.lblc.TabIndex = 15;
            this.lblc.Text = "c";
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lbl.Location = new System.Drawing.Point(44, 73);
            this.lbl.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(20, 24);
            this.lbl.TabIndex = 11;
            this.lbl.Text = "a";
            // 
            // txtA
            // 
            this.txtA.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtA.Location = new System.Drawing.Point(77, 70);
            this.txtA.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtA.Name = "txtA";
            this.txtA.Size = new System.Drawing.Size(83, 29);
            this.txtA.TabIndex = 12;
            this.txtA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtA_KeyPress);
            // 
            // txtX1
            // 
            this.txtX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtX1.Location = new System.Drawing.Point(273, 235);
            this.txtX1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtX1.Name = "txtX1";
            this.txtX1.ReadOnly = true;
            this.txtX1.Size = new System.Drawing.Size(83, 29);
            this.txtX1.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label4.Location = new System.Drawing.Point(240, 238);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 24);
            this.label4.TabIndex = 17;
            this.label4.Text = "X1";
            // 
            // txtX2
            // 
            this.txtX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtX2.Location = new System.Drawing.Point(273, 285);
            this.txtX2.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtX2.Name = "txtX2";
            this.txtX2.ReadOnly = true;
            this.txtX2.Size = new System.Drawing.Size(83, 29);
            this.txtX2.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label5.Location = new System.Drawing.Point(240, 288);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 24);
            this.label5.TabIndex = 19;
            this.label5.Text = "X2";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(244, 141);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(138, 39);
            this.btnCalcular.TabIndex = 21;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // btnMenu
            // 
            this.btnMenu.Font = new System.Drawing.Font("Berlin Sans FB", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu.Location = new System.Drawing.Point(15, 327);
            this.btnMenu.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(91, 30);
            this.btnMenu.TabIndex = 22;
            this.btnMenu.Text = "<-- Menú";
            this.btnMenu.UseVisualStyleBackColor = true;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // frmCuadratica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 371);
            this.Controls.Add(this.btnMenu);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.txtX2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtX1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtC);
            this.Controls.Add(this.lblc);
            this.Controls.Add(this.txtB);
            this.Controls.Add(this.lblb);
            this.Controls.Add(this.txtA);
            this.Controls.Add(this.lbl);
            this.Controls.Add(this.label1);
            this.Name = "frmCuadratica";
            this.Text = "Formula Cuadrática";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtB;
        private System.Windows.Forms.Label lblb;
        private System.Windows.Forms.TextBox txtC;
        private System.Windows.Forms.Label lblc;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.TextBox txtA;
        private System.Windows.Forms.TextBox txtX1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtX2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Button btnMenu;
    }
}