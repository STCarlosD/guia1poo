﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia2POO
{
    public partial class frmEmpleados : Form
    {
        public frmEmpleados()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            if (txtNombres.Text == "" || txtNombres.Text == null || txtNombres.Text == " " || txtApellidos.Text == "" || txtApellidos.Text == null || txtApellidos.Text == " " || txtSalarioBruto.Text == "" || txtSalarioBruto.Text == null || txtSalarioBruto.Text == " ")
            {
                MessageBox.Show("Llene correctamente los campos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                string nombres, apellidos;
                double salario_bruto, monto_descuento = 0, salario_neto = 0;

                salario_bruto = Convert.ToDouble(txtSalarioBruto.Text);
                if (rdbGerente.Checked)
                {
                    monto_descuento = salario_bruto * .2;
                    salario_neto = salario_bruto - monto_descuento;
                }
                else if (rdbSubgerente.Checked)
                {
                    monto_descuento = salario_bruto * .15;
                    salario_neto = salario_bruto - monto_descuento;
                }
                else if (rdbSecretaria.Checked)
                {
                    monto_descuento = salario_bruto * .05;
                    salario_neto = salario_bruto - monto_descuento;
                }

                txtMontoDescuento.Text = "$ " + monto_descuento;
                txtSalarioNeto.Text = "$ " + salario_neto;
            }
        }

        private void frmEmpleados_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            frmMenu frm = new frmMenu();
            frm.Show();
            this.Close();
        }

        private void txtSalarioBruto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
    }
}
