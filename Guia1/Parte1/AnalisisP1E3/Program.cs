﻿using System;

namespace AnalisisP1E3
{
    class Program
    {
        //Funcion para validar que se haya ingresado numero
        static bool validarNumero(string dato)
        {
            try
            {
                Convert.ToDouble(dato);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        //funcion para calculo de cubo
        static double imc(double numa, double numb)
        {
            return numb/Math.Pow(numa, 2);
        }
        //funcion para calculo de cubo
        static double cubo(double numa)
        {
            return Math.Pow(numa, 3);
        }   
        //funcion para la division
        static double division(double numa, double numb)
        {
            return numa / numb;
        }
        static void Main(string[] args)
        {
            string opc, numa = "0", numb = "0";
            bool flag = true, flagNum = false, flagNumB = false; ;           
            while (flag)
            {
                flagNum = false;
                flagNumB = false;
                Console.Clear();
                Console.WriteLine("\t\t CÁLCULOS VARIADOS");
                Console.WriteLine("\n a - División");
                Console.WriteLine("\n b - Obtener cubo");
                Console.WriteLine("\n c - Cálculo de IMC");
                Console.WriteLine("\n d - Salir");
                opc = Console.ReadLine();

                switch (opc)
                {
                    case "a":
                    case "A":
                        Console.Clear();
                        Console.WriteLine("\t\t a - División");
                        while (flagNum == false)
                        {
                            Console.WriteLine("\n Ingresa el dividendo: ");
                            numa = Console.ReadLine();

                            flagNum = validarNumero(numa);
                        }                        
                        while (flagNumB == false)
                        {
                            Console.WriteLine("\n Ingresa el divisor: ");
                            numb = Console.ReadLine();                            
                            if (validarNumero(numb))
                            {
                                if (Convert.ToDouble(numb) >= 0)
                                {
                                    flagNumB = true;
                                }
                            }
                        }
                        Console.WriteLine("\n El cociente de la división es: " + division(Convert.ToDouble(numa), Convert.ToDouble(numb)));
                        Console.WriteLine("\n Presiona cualquier tecla para regresar al menú.");
                        break;

                    case "b":
                    case "B":
                        Console.Clear();
                        Console.WriteLine("\n b - Obtener cubo");
                        while (flagNum == false)
                        {
                            Console.WriteLine("\n Ingresa el número a elevar al cubo: ");
                            numa = Console.ReadLine();
                            flagNum = validarNumero(numa);
                        }
                        Console.WriteLine("\n "+numa+"^3 es: " + cubo(Convert.ToDouble(numa)));
                        Console.WriteLine("\n Presiona cualquier tecla para regresar al menú.");
                        break;

                    case "C":
                    case "c":
                        Console.Clear();
                        Console.WriteLine("\n c - Cálculo de IMC");
                        while (flagNum == false)
                        {
                            Console.WriteLine("\n Ingresa tu altura (m): ");
                            numa = Console.ReadLine();
                            if (validarNumero(numa))
                            {
                                if (Convert.ToDouble(numa) >= 0)
                                {
                                    flagNum = true;
                                }
                            }
                        }
                        while (flagNumB == false)
                        {
                            Console.WriteLine("\n Ingresa tu peso (kg): ");
                            numb = Console.ReadLine();
                            if (validarNumero(numb))
                            {
                                if (Convert.ToDouble(numb) >= 0)
                                {
                                    flagNumB = true;
                                }
                            }
                        }
                        Console.WriteLine("\n Tu índice de masa corporal es: " + imc(Convert.ToDouble(numa), Convert.ToDouble(numb)));
                        Console.WriteLine("\n Presiona cualquier tecla para regresar al menú.");
                        break;

                    case "d":
                    case "D":
                        flag = false;
                        Environment.Exit(0);
                        break;

                    default:
                        Console.Clear();
                        Console.WriteLine("\n Opción no válida.");
                        Console.WriteLine("\n Presiona cualquier tecla para regresar al menú.");
                        break;
                }
                Console.ReadKey();
            }
        }
    }
}
