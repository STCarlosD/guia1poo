﻿using System;

namespace AnalisisP1E2
{
    class Program
    {
        //funcion para validar año que sea menor al actual
        static bool validarAnio(int anio)
        {
            DateTime fechaActual = DateTime.Today;
            if (anio >= fechaActual.Year)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        //funcion para validar entero y que no sea numero negativo
        static bool validarEntero(string dato)
        {
            try
            {
                Convert.ToInt32(dato);
                if(Convert.ToInt32(dato) < 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }
        //Procedimiento para calcular la edad
        static void CalculoEdad(int anio)
        {
            int edad;
            DateTime fechaActual = DateTime.Today;
            edad = Convert.ToInt32(fechaActual.Year) - anio;
            Console.WriteLine("Tu edad es: " + edad.ToString() + " años");
        }
        static void Main(string[] args)
        {
            string anio;
            bool flag = true;
            while(flag){
                Console.Clear();
                Console.WriteLine("\t\t PROGRAMA PARA CALCULAR EDAD DE UNA PERSONA");
                Console.WriteLine("\n Escribe tu año de nacimiento");
                anio = Console.ReadLine();
                if (validarEntero(anio))
                {
                    if (validarAnio(Convert.ToInt32(anio)))
                    {
                        CalculoEdad(Convert.ToInt32(anio));
                    }
                    else
                    {
                        Console.WriteLine("\n Debe ingresar un año menor al actual.");
                    }                    
                }
                else
                {
                    Console.WriteLine("\n Debe ingresar un año.");
                }
                Console.ReadKey();
            }                                    
        }
    }
}
