﻿using System;

namespace AnalisisP1E1
{
    class Program
    {
        static void Faren()
        {
            Double cel, far;
            Console.Write("Escribe los grados Celcius: ");
            cel = Convert.ToDouble(Console.ReadLine());
            far = cel * 9 / 5 + 32;
            Console.WriteLine("{0} grados celcius son {1} grados Fahrenheit", cel, far);
        }
        static void Celsi()
        {
            Double cel, far;
            Console.Write("Escribe los grados fahrenheit: ");
            far = Convert.ToDouble(Console.ReadLine());
            cel = (far - 32) * 5 / 9;
            Console.WriteLine("{0} grados Fahrenheit son {1} grados Celsius", far, cel);
        }
        static void CelsiKelvin()
        {
            Double cel, kel;
            Console.Write("Escribe los grados celsius: ");
            cel = Convert.ToDouble(Console.ReadLine());
            kel = cel + 273.15;
            Console.WriteLine("{0} grados Celsius son {1} grados Kelvin", cel, kel);
        }
        static void FahKelvin()
        {
            Double fah, kel;
            Console.Write("Escribe los grados Fahrenheit: ");
            fah = Convert.ToDouble(Console.ReadLine());
            kel = (5*(fah-32)/9)+273.15;
            Console.WriteLine("{0} grados Fahrenheit son {1} grados Kelvin", fah, kel);
        }
        static void Main(string[] args)
        {
            string opc;
            Console.WriteLine("\t\t PROGRAMA PARA CONVERTIR TEMPERATURAS");
            Console.WriteLine("\n Escoge el tipo de conversion:");
            Console.WriteLine("\n F - Convertir de celsius a fahrenheit");
            Console.WriteLine("\n C - Convertir de fahrenheit a celsius");
            Console.WriteLine("\n FK - Convertir de fahrenheit a kelvin");
            Console.WriteLine("\n CK - Convertir de celsius a kelvin");
            Console.WriteLine("\n Opción: ");

            opc = Console.ReadLine();
            switch (opc)
            {
                case "F":
                case "f":
                    Console.WriteLine("\n Conversion a F");
                    Faren();
                    break;

                case "C":
                case "c":
                    Console.WriteLine("\n Conversion a C");
                    Celsi();
                    break;
                case "CK":
                case "ck":
                    Console.WriteLine("\n Conversion de C a K");
                    CelsiKelvin();
                    break;
                case "FK":
                case "fk":
                    Console.WriteLine("\n Conversion de F a K");
                    FahKelvin();
                    break;
                default:
                    Console.WriteLine("\n Opción no válida");
                    break;
            }

            Console.ReadKey();

        }
    }
}
