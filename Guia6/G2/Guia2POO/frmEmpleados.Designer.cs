﻿namespace Guia2POO
{
    partial class frmEmpleados
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Nombre = new System.Windows.Forms.Label();
            this.lblApellidos = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.rdbGerente = new System.Windows.Forms.RadioButton();
            this.rdbSubgerente = new System.Windows.Forms.RadioButton();
            this.rdbSecretaria = new System.Windows.Forms.RadioButton();
            this.txtNombres = new System.Windows.Forms.TextBox();
            this.txtApellidos = new System.Windows.Forms.TextBox();
            this.txtSalarioBruto = new System.Windows.Forms.TextBox();
            this.txtMontoDescuento = new System.Windows.Forms.TextBox();
            this.txtSalarioNeto = new System.Windows.Forms.TextBox();
            this.btnMenu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(354, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Descuentos";
            // 
            // Nombre
            // 
            this.Nombre.AutoSize = true;
            this.Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.Nombre.Location = new System.Drawing.Point(116, 160);
            this.Nombre.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.Nombre.Name = "Nombre";
            this.Nombre.Size = new System.Drawing.Size(88, 24);
            this.Nombre.TabIndex = 1;
            this.Nombre.Text = "Nombres";
            this.Nombre.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblApellidos
            // 
            this.lblApellidos.AutoSize = true;
            this.lblApellidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.lblApellidos.Location = new System.Drawing.Point(578, 160);
            this.lblApellidos.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblApellidos.Name = "lblApellidos";
            this.lblApellidos.Size = new System.Drawing.Size(88, 24);
            this.lblApellidos.TabIndex = 2;
            this.lblApellidos.Text = "Apellidos";
            this.lblApellidos.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 262);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 24);
            this.label4.TabIndex = 3;
            this.label4.Text = "Salario bruto";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 352);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(157, 24);
            this.label5.TabIndex = 4;
            this.label5.Text = "Monto descuento";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(36, 413);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 24);
            this.label6.TabIndex = 5;
            this.label6.Text = "Salario neto";
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(516, 493);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(138, 39);
            this.btnCalcular.TabIndex = 6;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // rdbGerente
            // 
            this.rdbGerente.AutoSize = true;
            this.rdbGerente.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.rdbGerente.Location = new System.Drawing.Point(614, 262);
            this.rdbGerente.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.rdbGerente.Name = "rdbGerente";
            this.rdbGerente.Size = new System.Drawing.Size(96, 28);
            this.rdbGerente.TabIndex = 7;
            this.rdbGerente.TabStop = true;
            this.rdbGerente.Text = "Gerente";
            this.rdbGerente.UseVisualStyleBackColor = true;
            this.rdbGerente.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // rdbSubgerente
            // 
            this.rdbSubgerente.AutoSize = true;
            this.rdbSubgerente.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.rdbSubgerente.Location = new System.Drawing.Point(614, 335);
            this.rdbSubgerente.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.rdbSubgerente.Name = "rdbSubgerente";
            this.rdbSubgerente.Size = new System.Drawing.Size(127, 28);
            this.rdbSubgerente.TabIndex = 8;
            this.rdbSubgerente.TabStop = true;
            this.rdbSubgerente.Text = "Subgerente";
            this.rdbSubgerente.UseVisualStyleBackColor = true;
            this.rdbSubgerente.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // rdbSecretaria
            // 
            this.rdbSecretaria.AutoSize = true;
            this.rdbSecretaria.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.rdbSecretaria.Location = new System.Drawing.Point(614, 413);
            this.rdbSecretaria.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.rdbSecretaria.Name = "rdbSecretaria";
            this.rdbSecretaria.Size = new System.Drawing.Size(112, 28);
            this.rdbSecretaria.TabIndex = 9;
            this.rdbSecretaria.TabStop = true;
            this.rdbSecretaria.Text = "Secretaria";
            this.rdbSecretaria.UseVisualStyleBackColor = true;
            this.rdbSecretaria.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // txtNombres
            // 
            this.txtNombres.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtNombres.Location = new System.Drawing.Point(304, 158);
            this.txtNombres.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtNombres.Name = "txtNombres";
            this.txtNombres.Size = new System.Drawing.Size(180, 29);
            this.txtNombres.TabIndex = 10;
            this.txtNombres.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtApellidos
            // 
            this.txtApellidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtApellidos.Location = new System.Drawing.Point(712, 157);
            this.txtApellidos.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtApellidos.Name = "txtApellidos";
            this.txtApellidos.Size = new System.Drawing.Size(180, 29);
            this.txtApellidos.TabIndex = 11;
            this.txtApellidos.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtSalarioBruto
            // 
            this.txtSalarioBruto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtSalarioBruto.Location = new System.Drawing.Point(260, 261);
            this.txtSalarioBruto.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtSalarioBruto.Name = "txtSalarioBruto";
            this.txtSalarioBruto.Size = new System.Drawing.Size(180, 29);
            this.txtSalarioBruto.TabIndex = 12;
            this.txtSalarioBruto.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            this.txtSalarioBruto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalarioBruto_KeyPress);
            // 
            // txtMontoDescuento
            // 
            this.txtMontoDescuento.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtMontoDescuento.Location = new System.Drawing.Point(260, 349);
            this.txtMontoDescuento.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtMontoDescuento.Name = "txtMontoDescuento";
            this.txtMontoDescuento.ReadOnly = true;
            this.txtMontoDescuento.Size = new System.Drawing.Size(180, 29);
            this.txtMontoDescuento.TabIndex = 13;
            this.txtMontoDescuento.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // txtSalarioNeto
            // 
            this.txtSalarioNeto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.txtSalarioNeto.Location = new System.Drawing.Point(260, 409);
            this.txtSalarioNeto.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtSalarioNeto.Name = "txtSalarioNeto";
            this.txtSalarioNeto.ReadOnly = true;
            this.txtSalarioNeto.Size = new System.Drawing.Size(180, 29);
            this.txtSalarioNeto.TabIndex = 14;
            this.txtSalarioNeto.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // btnMenu
            // 
            this.btnMenu.Font = new System.Drawing.Font("Berlin Sans FB", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenu.Location = new System.Drawing.Point(15, 509);
            this.btnMenu.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(91, 30);
            this.btnMenu.TabIndex = 17;
            this.btnMenu.Text = "<-- Menú";
            this.btnMenu.UseVisualStyleBackColor = true;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // frmEmpleados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 553);
            this.Controls.Add(this.btnMenu);
            this.Controls.Add(this.txtSalarioNeto);
            this.Controls.Add(this.txtMontoDescuento);
            this.Controls.Add(this.txtSalarioBruto);
            this.Controls.Add(this.txtApellidos);
            this.Controls.Add(this.txtNombres);
            this.Controls.Add(this.rdbSecretaria);
            this.Controls.Add(this.rdbSubgerente);
            this.Controls.Add(this.rdbGerente);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblApellidos);
            this.Controls.Add(this.Nombre);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "frmEmpleados";
            this.Text = "Empleados";
            this.Load += new System.EventHandler(this.frmEmpleados_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Nombre;
        private System.Windows.Forms.Label lblApellidos;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.RadioButton rdbGerente;
        private System.Windows.Forms.RadioButton rdbSubgerente;
        private System.Windows.Forms.RadioButton rdbSecretaria;
        private System.Windows.Forms.TextBox txtNombres;
        private System.Windows.Forms.TextBox txtApellidos;
        private System.Windows.Forms.TextBox txtSalarioBruto;
        private System.Windows.Forms.TextBox txtMontoDescuento;
        private System.Windows.Forms.TextBox txtSalarioNeto;
        private System.Windows.Forms.Button btnMenu;
    }
}

