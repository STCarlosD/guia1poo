﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia2POO
{
    public partial class frmEjemplo5 : Form
    {
        public frmEjemplo5()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listbArreglo.Items[0].ToString() == "" || listbArreglo.Items[0].ToString()  == null || listbArreglo.Items[0].ToString() == " ")
            {
                MessageBox.Show("Llene correctamente los campos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                int mayorneg = -1000;
                for (int i = 0; i < listbArreglo.Items.Count; i++)
                {
                    string valor = listbArreglo.Items[i].ToString();
                    int numero = int.Parse(valor);
                    if (numero < 0 && numero % 2 == 0)
                    {
                        if (numero > mayorneg)

                        {

                            mayorneg = numero;
                            txtCalculo1.Text = mayorneg.ToString();
                        }
                    }
                    else
                    {
                        txtCalculo1.Text = "No hay números negativos pares";
                    }
                }


                double cantidadnumeros = listbArreglo.Items.Count;
                double cantidadceros = 0;
                double porcentaje = 0;
                for (int i = 0; i < listbArreglo.Items.Count; i++)
                {
                    string valor = listbArreglo.Items[i].ToString();
                    int numero = int.Parse(valor);
                    if (numero == 0)
                    {
                        cantidadceros = cantidadceros + 1;
                    }
                }
                porcentaje = (cantidadceros / cantidadnumeros) * 100;
                txtCalculo2.Text = porcentaje.ToString() + "%";


                double prom;
                double cantidadimpares = 0;
                double suma = 0;
                for (int i = 0; i < listbArreglo.Items.Count; i++)
                {
                    string valor = listbArreglo.Items[i].ToString();
                    int numero = int.Parse(valor);
                    if (numero > 0 && numero % 2 != 0)
                    {
                        suma = suma + numero;
                        cantidadimpares = cantidadimpares + 1;
                    }
                }
                prom = suma / cantidadimpares;
                txtCalculo3.Text = prom.ToString();


                int mayor = 0;
                for (int i = 0; i < listbArreglo.Items.Count; i++)
                {
                    string valor = listbArreglo.Items[i].ToString();
                    int numero = int.Parse(valor);
                    if (numero > 0 && numero % 2 == 0)
                    {
                        if (numero > mayor)
                            mayor = numero;
                    }
                }
                txtCalculo4.Text = mayor.ToString();
            }
        }
       

        private void frmEjemplo5_Load(object sender, EventArgs e)
        {

        }

        private void txtNumero_Enter(object sender, EventArgs e)
        {
            //listbArreglo.Items.Add(txtNumero.Text);
            //txtNumero.Clear();
            //txtNumero.Focus();
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && !(e.KeyChar == 13) && !(e.KeyChar == 45))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            if (e.KeyChar == 13)//Enter
            {
                listbArreglo.Items.Add(txtNumero.Text);
                txtNumero.Clear();
            }
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            frmMenu frm = new frmMenu();
            frm.Show();
            this.Close();
        }
    }
}
