﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia2POO
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void btnConversiones_Click(object sender, EventArgs e)
        {
            frmConversiones frm = new frmConversiones();
            this.Hide();
            frm.Show();
        }

        private void btnEmpleados_Click(object sender, EventArgs e)
        {
            frmEmpleados frm = new frmEmpleados();
            this.Hide();
            frm.Show();
        }

        private void btnFormula_Click(object sender, EventArgs e)
        {
            frmCuadratica frm = new frmCuadratica();
            this.Hide();
            frm.Show();
        }

        private void btnEjemplo_Click(object sender, EventArgs e)
        {
            frmEjemplo5 frm = new frmEjemplo5();
            this.Hide();
            frm.Show();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
