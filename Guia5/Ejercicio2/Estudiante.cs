﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Estudiante
    {
        string nombre;
        string carnet;
        DateTime fechaNacimiento;
        string correo;
        string responsables;

        string materia;
        double n1;
        double n2;
        double n3;
        double promedio;

        public string Nombre { get => nombre; set => nombre = value; }
        public string Carnet { get => carnet; set => carnet = value; }
        public DateTime FechaNacimiento { get => fechaNacimiento; set => fechaNacimiento = value; }
        public string Correo { get => correo; set => correo = value; }
        public string Responsables { get => responsables; set => responsables = value; }
        public string Materia { get => materia; set => materia = value; }
        public double N1 { get => n1; set => n1 = value; }
        public double N2 { get => n2; set => n2 = value; }
        public double N3 { get => n3; set => n3 = value; }
        public double Promedio { get => promedio; set => promedio = value; }
    }
}
