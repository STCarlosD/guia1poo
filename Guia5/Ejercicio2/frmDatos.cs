﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Ejercicio2
{
    public partial class frmDatos : Form
    {
        public frmDatos()
        {
            InitializeComponent();
        }

        internal static Estudiante est = new Estudiante();

        private bool validarCampos()
        {
            bool validado = true;
            if (txtNombre.Text == "") //vefica que no quede vacío el campo
            {
                validado = false; //si está vacío validado es falso
                errorProvider1.SetError(txtNombre, "Ingresar nombre");                
            }
            if (!validarCarnet(txtCarnet.Text))
            {
                validado = false;
                errorProvider1.SetError(txtCarnet, "Ingrese el carné correctamente");
            }
            if (!validarCorreo(txtCorreo.Text))
            {
                validado = false;                
                errorProvider1.SetError(txtCorreo, "Ingrese el correo correctamente");
            }
            if (cboResponsable.SelectedIndex == -1)
            {
                validado = false;
                errorProvider1.SetError(cboResponsable, "Seleccione responsable");
            }
            DateTime fechaNacimiento = dtpFechaNac.Value;

            //se valida si el año seleccionado es mayor al actual y se manda mensaje de error
            if (fechaNacimiento.Year > System.DateTime.Now.Year)
            {
                validado = false;
                errorProvider1.SetError(dtpFechaNac, "Año de nacimiento no valido");
            }
            return validado;
        }

        private void btnContinuar_Click(object sender, EventArgs e)
        {
            borrarMesaje();
            if (validarCampos())
            {
                est.Nombre = txtNombre.Text;
                est.Carnet = txtCarnet.Text;
                est.Correo = txtCorreo.Text;
                est.Responsables = cboResponsable.SelectedItem.ToString();
                est.FechaNacimiento = dtpFechaNac.Value;

                frmNotas frm = new frmNotas();
                this.Hide();
                frm.Show();
            }
        }
        private void borrarMesaje()
        {
            //borra los mensajes para que no se muestren y pueda limpiar
            errorProvider1.SetError(txtCarnet, "");
            errorProvider1.SetError(txtCorreo, "");
            errorProvider1.SetError(txtNombre, "");
            errorProvider1.SetError(cboResponsable, "");
            errorProvider1.SetError(dtpFechaNac, "");
        }

        public static bool validarCorreo(string correo)
        {            
            string expresion = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"
                                   + "@"
                                   + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))\z";
            if (Regex.IsMatch(correo, expresion))
            {            
            if (Regex.Replace(correo, expresion, string.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool validarCarnet(string carnet)
        {
            string expresion = "[a-zA-Z]{2}[0-9]{6}";
            if (Regex.IsMatch(carnet, expresion))
            {
                if (Regex.Replace(carnet, expresion, string.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cboResponsable.Items.Add("Mamá");
            cboResponsable.Items.Add("Papá");
        }
    }
}
