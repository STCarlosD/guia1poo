﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace G3_Ejercicio_05
{
    public partial class frmRecibe : Form
    {
        public List<Persona> PersonaRecibe = null;
        public List<Persona> PersonaFiltro = null;
        public frmRecibe()
        {
            InitializeComponent();
        }
        private void actualizarGrid() //función que llena el DGV del formulario 2
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = PersonaRecibe;
        }

        private void btnLlenar_Click(object sender, EventArgs e)
        {
            actualizarGrid();
        }

        private void btnFiltro_Click(object sender, EventArgs e)
        {
            string palabra = txtNombre.Text;
            for (int i = 0; i < PersonaFiltro.Count(); i++)
            {
                //myString.StartWith("someString");
                if (!PersonaFiltro[i].Nombre.StartsWith(palabra))
                {
                    PersonaFiltro.Remove(PersonaFiltro[i]);
                }
            }
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = PersonaFiltro;
        }
    }
}
