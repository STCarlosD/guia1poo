﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace G3_Ejercicio_002
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private List<alumno> Alumnos = new List<alumno>();

        private void actualizarGrid()
        {
            dgvAlumnos.DataSource = null;
            dgvAlumnos.DataSource = Alumnos;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            alumno al = new alumno();
            al.Nombre = txtNombre.Text;
            al.Apellido = txtApellidos.Text;
            al.Materia = txtMateria.Text;
            al.Carnet = txtCarnet.Text;
            al.Nota1 = Convert.ToDouble(txtNota1.Text);
            al.Nota2 = Convert.ToDouble(txtNota2.Text);
            al.Nota3 = Convert.ToDouble(txtNota3.Text);
            al.Promedio = (al.Nota1 + al.Nota2 + al.Nota3) / 3;

            Alumnos.Add(al);
            actualizarGrid();
            limpiarCampos();

        }
        private void limpiarCampos()
        {
            txtNombre.Clear();
            txtApellidos.Clear();
            txtCarnet.Clear();
            txtMateria.Clear();
            txtNota1.Clear();
            txtNota2.Clear();
            txtNota3.Clear();
        }
    }
}
