﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G3_Ejercicio_01
{
    class Cliente
    {
        private string nombres;
        private string apellidos;
        private string dui;
        private string tipoCuenta;
        private string numeroCuenta;
        private string nit;
        private double monto;
        private string sucursal;

        public string Nombres { get => nombres; set => nombres = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public string Dui { get => dui; set => dui = value; }
        public string TipoCuenta { get => tipoCuenta; set => tipoCuenta = value; }
        public string NumeroCuenta { get => numeroCuenta; set => numeroCuenta = value; }
        public string Nit { get => nit; set => nit = value; }
        public double Monto { get => monto; set => monto = value; }
        public string Sucursal { get => sucursal; set => sucursal = value; }
    }
}
