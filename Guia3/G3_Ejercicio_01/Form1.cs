﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace G3_Ejercicio_01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private List<Cliente> Clientes = new List<Cliente>();



        private void actualizarGrid()
        {
            dgvClientes.DataSource = null;
            dgvClientes.DataSource = Clientes;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cliente cli = new Cliente();
            cli.Nombres = txtNombres.Text;
            cli.Apellidos = txtApellidos.Text;
            cli.Dui = txtDui.Text;
            cli.Nit = txtNit.Text;
            cli.Monto = Convert.ToDouble(txtMonto.Text);
            cli.TipoCuenta = cboTipoCuenta.SelectedItem.ToString();
            cli.Sucursal = cboSucursal.SelectedItem.ToString();
            int i = Clientes.Count() + 1;
            string numeroCuenta = "sinllenar";
            if (cboTipoCuenta.SelectedItem.ToString() == "Corriente")
            {
                numeroCuenta = "CC" + string.Format("{0:00000}", i);
            }
            if (cboTipoCuenta.SelectedItem.ToString() == "Ahorros")
            {
                numeroCuenta = "CA" + string.Format("{0:00000}", i);
            }
            if (cboTipoCuenta.SelectedItem.ToString() == "Plazos")
            {
                numeroCuenta = "CP" + string.Format("{0:00000}", i);
            }
            cli.NumeroCuenta = numeroCuenta;

            Clientes.Add(cli);
            actualizarGrid();
            limpiarTxt();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cboSucursal.Items.Add("Metrocentro");
            cboSucursal.Items.Add("Unicentro");
            cboSucursal.Items.Add("Multiplaza");
            cboSucursal.Items.Add("Plaza Mundo");
            cboSucursal.Items.Add("Cascadas");

            cboTipoCuenta.Items.Add("Corriente");
            cboTipoCuenta.Items.Add("Ahorros");
            cboTipoCuenta.Items.Add("Plazos");
        }
        private void limpiarTxt()
        {
            txtApellidos.Clear();
            txtNombres.Clear();
            txtNit.Clear();
            txtDui.Clear();
            txtMonto.Clear();
            cboSucursal.SelectedIndex = -1;
            cboTipoCuenta.SelectedIndex = -1;
        }
    }
}
